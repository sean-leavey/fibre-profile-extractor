#!/usr/bin/env python3

import os
import io
import sys
import logging
import argparse
import textwrap
import collections

from fibrecnv import PROG, SYNOPSIS, MANPAGE
from fibrecnv import extract

def enable_verbose_logs(level):
    handler = logging.StreamHandler()
    handler.setFormatter(logging.Formatter('%(message)s'))
    logger = logging.getLogger()
    logger.addHandler(handler)
    logger.setLevel(level)

def find_fibre_files_by_id(input_dir, ids):
    # find files in data directory
    fibre_files = extract.find_fibre_files(input_dir)
    fibre_identifiers = fibre_files.keys()

    # check that all identifiers to convert were found
    for identifier in ids:
        if identifier not in fibre_identifiers:
            sys.exit("Could not find files using identifier \"{0}\"".format(
            identifier))

    return fibre_files

class Cmd(object):
    """Base class for commands"""

    cmd = ""

    def __init__(self):
        """Initialize argument parser"""
        self.parser = argparse.ArgumentParser(
            prog="{} {}".format(PROG, self.cmd),
            description=self.__doc__.strip()
        )
    def parse_args(self, args):
        """Parse arguments and returned ArgumentParser Namespace object"""
        return self.parser.parse_args(args)
    def __call__(self, args):
        """Take Namespace object as input and execute command"""
        pass

class List(Cmd):
    """List fibre profile files in a given directory"""

    cmd = "list"

    def __init__(self):
        Cmd.__init__(self)
        self.parser.add_argument("input_dir",
                                 help="directory containing the fibre profile "
                                      "files")
        self.parser.add_argument("-v", "--verbose", action="store_true",
                                 help="enable verbose output")
        self.parser.add_argument("-V", "--very-verbose", action="store_true",
                                 help="enable very verbose output")

    def __call__(self, args):
        if args.verbose:
            enable_verbose_logs(logging.INFO)
        elif args.very_verbose:
            enable_verbose_logs(logging.DEBUG)

        input_dir = os.path.abspath(str(args.input_dir))

        if not extract.dir_is_read_valid(input_dir):
            sys.exit("Input directory is not readable")

        # find identifiers and corresponding files in data directory
        fibre_files = extract.find_fibre_files(input_dir)

        # get valid identifiers
        valid_ids = [k for k in fibre_files.keys() \
        if extract.validate_fibre_file_group(fibre_files[k])]

        if len(valid_ids):
            # print them
            print("Found the following identifiers:")
            for ident in valid_ids:
                print(ident)
        else:
            print("Found no valid indentifiers")

class Convert(Cmd):
    """Convert fibre profile files into single file"""

    cmd = "convert"

    def __init__(self):
        Cmd.__init__(self)
        self.parser.add_argument("input_dir",
                                 help="directory containing the fibre profile "
                                      "files")
        self.parser.add_argument("-o", "--output_dir",
                                 help="directory to store output file",
                                 default=None)
        self.parser.add_argument("-i", "--identifier",
                                 help="identifier to extract "
                                      "(leave blank for all)",
                                 default=None)
        self.parser.add_argument("-f", "--force", action="store_true",
                                 help="overwrite existing output files")
        self.parser.add_argument("-v", "--verbose", action="store_true",
                                 help="enable verbose output")
        self.parser.add_argument("-V", "--very-verbose", action="store_true",
                                 help="enable very verbose output")

    def __call__(self, args):
        if args.verbose:
            enable_verbose_logs(logging.INFO)
        elif args.very_verbose:
            enable_verbose_logs(logging.DEBUG)

        input_dir = os.path.abspath(str(args.input_dir))

        if not extract.dir_is_read_valid(input_dir):
            sys.exit("Input directory is not readable")

        if args.output_dir is None:
            logging.getLogger().info("Using input directory to store output file")
            output_dir = input_dir
        else:
            output_dir = os.path.abspath(str(args.output_dir))

        if not extract.dir_is_write_valid(output_dir):
            sys.exit("Output directory is not writeable")

        # find identifiers and corresponding files in data directory
        fibre_files = extract.find_fibre_files(input_dir)
        fibre_identifiers = fibre_files.keys()

        # build list of identifiers to convert
        ids_to_extract = []
        if args.identifier is None:
            ids_to_extract.extend(fibre_identifiers)
        else:
            ids_to_extract.append(str(args.identifier))

        # check that all identifiers to convert were found
        for identifier in ids_to_extract:
            if identifier not in fibre_identifiers:
                sys.exit("Could not find files using identifier \"{0}\"".format(
                identifier))

        # loop over output files first to check if any of them exist already
        files_existing = []
        for identifier in ids_to_extract:
            output_file = os.path.join(output_dir, "{0}.csv".format(identifier))

            if not args.force and os.path.exists(output_file):
                files_existing.append(output_file)

        if len(files_existing):
            sys.exit("The following files already exist (specify -f to force "
                     "overwrite): {0}".format(", ".join(files_existing)))

        logging.getLogger().info("Extracting data for the following "
                                 "identifiers: {0}".format(
                                    ", ".join(ids_to_extract)
                                 ))

        # loop over output files again, this time writing the data
        for identifier in ids_to_extract:
            output_file = os.path.join(output_dir, "{0}.csv".format(identifier))

            if os.path.exists(output_file):
                if not args.force:
                    # cannot overwrite existing file
                    sys.exit("File {0} already exists (specify -f to force "
                             "overwrite)".format(output_file))
                else:
                    logging.getLogger().info("Overwriting {0}".format(output_file))
            else:
                logging.getLogger().info("Writing {0}".format(output_file))

            # write data to output file in CSV format
            extract.write_csv_data(output_file,
                extract.extract_data(input_dir, fibre_files[identifier]))

class Help(Cmd):
    """Print manpage or command help (also "-h" after command)"""

    cmd = "help"

    def __init__(self):
        Cmd.__init__(self)
        self.parser.add_argument('cmd', nargs='?',
                                 help="command")

    def __call__(self, args):
        if args.cmd:
            get_func(args.cmd).parser.print_help()
        else:
            print(MANPAGE.format(cmds=format_commands(man=True)))

CMDS = collections.OrderedDict([
    ("list", List),
    ("convert", Convert),
    ("help", Help),
    ])

ALIAS = {
    "--help": "help",
    "-h": "help",
    }

def format_commands(man=False):
    prefix = " " * 8
    wrapper = textwrap.TextWrapper(
        width=70,
        initial_indent=prefix,
        subsequent_indent=prefix,
        )
    with io.StringIO() as f:
        for name, func in CMDS.items():
            if man:
                fo = func()
                usage = fo.parser.format_usage()[len("usage: {} ".format(PROG)):].strip()
                desc = wrapper.fill('\n'.join([l.strip() for l in fo.parser.description.splitlines() if l]))
                f.write('  {}  \n'.format(usage))
                f.write(desc+'\n')
                f.write('\n')
            else:
                desc = func.__doc__.splitlines()[0]
                f.write('  {:10}{}\n'.format(name, desc))
        output = f.getvalue()
    return output.rstrip()

def get_func(cmd):
    if cmd in ALIAS:
        cmd = ALIAS[cmd]
    try:
        return CMDS[cmd]()
    except KeyError:
        print("Unknown command:", cmd, file=sys.stderr)
        print("See \"help\" for usage.", file=sys.stderr)
        sys.exit(1)

def main():
    if len(sys.argv) < 2:
        print("Command not specified.", file=sys.stderr)
        print("usage: " + SYNOPSIS, file=sys.stderr)
        print(file=sys.stderr)
        print(format_commands(), file=sys.stderr)
        sys.exit(1)
    cmd = sys.argv[1]
    args = sys.argv[2:]
    func = get_func(cmd)
    func(func.parse_args(args))

if __name__ == "__main__":
    main()
