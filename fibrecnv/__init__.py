PROG = "fibrecnv"
DESC = "Fibre profile data converter utility"
VERS = "0.8.1"
URL = "https://git.ligo.org/sean-leavey/fibre-profile-extractor"

SYNOPSIS = "{} <command> [<args>...]".format(PROG)
MANPAGE = """
NAME
  {prog} - {desc} (v{vers})
  {url}

SYNOPSIS
  {synopsis}

DESCRIPTION

  Command line utility to extract fibre profile information as generated
  by the IGR fibre profiler LabVIEW script. This program finds fibre files
  corresponding to the top, middle and bottom measurements, extracts the
  relevant length and thickness columns and saves them in a single file.
  This conversion operation can be performed on a single set of files
  sharing the same identifier or on an entire directory.

COMMANDS

{{cmds}}

AUTHOR
    Sean Leavey <sean.leavey@glasgow.ac.uk>
""".format(prog=PROG,
           desc=DESC,
           vers=VERS,
           url=URL,
           synopsis=SYNOPSIS,
           ).strip()
