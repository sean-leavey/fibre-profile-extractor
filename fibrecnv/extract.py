import os
import os.path
import re
import logging
import csv

re1 = '(0[1-9]|[12][0-9]|3[01])' # day
re2 = '_' # underscore
re3 = '(0[1-9]|1[012])' # month
re4 = '_' # underscore
re5 = '([0-9]{4})' # year
re6 = '\\s+' # space
re7 = '((?:[a-z0-9\\s_+]+))' # description
re8 = '\\s+' # space
re9 = '(Bottom Neck|Middle|Top Neck)' # location
re10 = '\\.xls' # extension

file_name_regex = re.compile(re1+re2+re3+re4+re5+re6+re7+re8+re9+re10,
                  re.IGNORECASE|re.DOTALL)

def dir_is_read_valid(path):
    """Check if the specified path is write valid"""
    return os.access(path, os.R_OK)

def dir_is_write_valid(path):
    """Check if the specified path is write valid"""
    return os.access(path, os.W_OK)

def find_fibre_files(directory):
    """Finds fibre data files in the specified directory"""

    # dict to hold file identifiers
    identifiers = {}

    for filename in os.listdir(directory):
        matches = file_name_regex.match(filename)

        if matches is None:
            # no matches for this file
            continue

        # get identifier
        identifier = str(matches.group(4))
        # get type
        meas_type = str(matches.group(5))

        # create dict if necessary
        if identifier not in identifiers.keys():
            identifiers[identifier] = {}

        # store file in indentifiers dict
        identifiers[identifier][meas_type] = filename

        logging.getLogger().debug("Found {0} ({1})".format(identifier,
                                                          meas_type))

    return identifiers

def validate_fibre_file_group(fibre_files):
    """Checks whether the required fibre files exist in a fibre file group"""

    required_fibre_files = ["Top Neck", "Middle", "Bottom Neck"]

    # check all of the relevant files are present
    return set(required_fibre_files) == set(fibre_files.keys())

def extract_data(fibre_dir, fibre_files):
    """Generates rows from the top, middle and bottom fibre files"""

    if not validate_fibre_file_group(fibre_files):
        raise Exception("Could not find all necessary fibre files")

    # proper filepaths
    filepath_top = os.path.join(fibre_dir, fibre_files["Top Neck"])
    filepath_mid = os.path.join(fibre_dir, fibre_files["Middle"])
    filepath_bot = os.path.join(fibre_dir, fibre_files["Bottom Neck"])

    # row counter
    row_count = 0

    # return the appropriate columns in each dataset
    logging.getLogger().debug("Extracting columns 1 and 2 from top file")
    logging.getLogger().info("Top file data starts at row {0}".format(row_count + 1))
    for row in csv.reader(open(filepath_top, 'r'), delimiter='\t'):
        yield [row[0], row[1]]
        row_count += 1
    
    logging.getLogger().debug("Extracting columns 1 and 4 from middle file")
    logging.getLogger().info("Middle file data starts at row {0}".format(row_count + 1))
    for row in csv.reader(open(filepath_mid, 'r'), delimiter='\t'):
        yield [row[0], row[3]]
        row_count += 1
    
    logging.getLogger().debug("Extracting columns 1 and 2 from bottom file")
    logging.getLogger().info("Bottom file data starts at row {0}".format(row_count + 1))
    for row in csv.reader(open(filepath_bot, 'r'), delimiter='\t'):
        yield [row[0], row[1]]
        row_count += 1
    
    logging.getLogger().info("Output file contains {0} rows".format(row_count))

def write_csv_data(target_file, data):
    """Writes comma separated data from an iterator into the target file"""
    with open(target_file, "w") as f:
        # format and write rows to file
        f.writelines(["{0},{1}{2}".format(row[0], row[1], os.linesep) for row in data])
