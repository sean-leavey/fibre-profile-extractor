# Fibre profile data converter utility
This is a script to convert the format of the IGR fibre profiler's data files into a more understandable format. The fibre profiler outputs three different files representing the profiles of the top, middle and bottom sections of a single fibre. The columns in the middle file are arranged differently to those of the top and bottom. The file naming convention is also somewhat odd.

This tool searches the specified directory for appropriate fibre profile files, works out what they are, and combines them into one file that uses the same label as the original files. For example, the profiler might output the following files in directory `/home/sean/fibres`:
  - `10_02_2017 20170210_A0 Bottom Neck.xls`
  - `10_02_2017 20170210_A0 Middle.xls`
  - `10_02_2017 20170210_A0 Top Neck.xls`

Here, the middle part, `20170210_A0`, is the identifier that the user provides to the LabVIEW script that produces those files. The last part, `Bottom Neck`, `Middle` and `Top Neck`, designates the type of file. This utility will use the identifier to name the new file, by default stored in the same directory as the source files, which contains the appropriate length and thickness columns from the three source files.
## Prerequisites
  - Python 3.5

## Use
Navigate to the directory containing `fibrecnv`. Open up a terminal or command prompt and type:
```
$ python3 -m fibrecnv
```
This will list the usage instructions:
```
usage: fibrecnv <command> [<args>...]

  list      List fibre profile files in a given directory
  convert   Convert fibre profile files into single file
  help      Print manpage or command help (also "-h" after command)
```
As per the instructions, you can use the commands `list`, `convert` and `help`. To learn more about the first two commands, type `python3 -m fibrecnv help list` or `python3 -m fibrecnv help convert`. To just convert all of the fibre files in a directory, you would run `python3 -m fibrecnv convert /path/to/fibre/files`. There are optional parameters you can pass to `convert`, though, to turn on additional information output or to specify only a single identifier to convert - take a look at the help pages.

### Examples
Say you have fibre profiler data stored in the directory `/home/sean/fibres`. It contains three files:
  - `10_02_2017 20170210_A0 Bottom Neck.xls`
  - `10_02_2017 20170210_A0 Middle.xls`
  - `10_02_2017 20170210_A0 Top Neck.xls`
  - `11_02_2017 20170211_A1 Bottom Neck.xls`
  - `11_02_2017 20170211_A1 Middle.xls`
  - `11_02_2017 20170211_A1 Top Neck.xls`
To detect collections of profiler data, you would run the command:
```
$ python3 -m fibrecnv list /home/sean/fibres
```
This will output:
```
Found the following identifiers:
20170210_A0
20170211_A1
```
To convert them, execute the command:
```
$ python3 -m fibrecnv convert /home/sean/fibres
```
This will output nothing by default, but there will be new files called `20170210_A0.csv` and `20170211_A1.csv` in the same directory as the the input data. To view useful information output during the conversion process, add the `-v` flag to your command, after `fibrecnv`:
```
$ python3 -m fibrecnv -v convert /home/sean/fibres
```
This will output:
```
Using input directory to store output file
Extracting data for the following identifiers: 20170211_A1, 20170210_A0
Writing /home/sean/fibres/20170211_A1.csv
Top file data starts at row 1
Middle file data starts at row 1917
Bottom file data starts at row 2086
Output file contains 3522 rows
Writing /home/sean/fibres/20170210_A0.csv
Top file data starts at row 1
Middle file data starts at row 1917
Bottom file data starts at row 2086
Output file contains 3522 rows
```
You can enable _very_ verbose output by using the `-V` flag.

Note that if you convert the fibre files more than once, you will get an error message stating that output files already exist:
```
Using input directory to store output file
The following files already exist (specify -f to force overwrite): /home/sean/fibres/20170210_A0.csv, /home/sean/Workspace/fibres/20170211_A0.csv
```
You can force `fibrecnv` to overwrite existing output files using the `-f` flag:
```
$ python3 -m fibrecnv -f convert /home/sean/fibres
```
You can also combine the useful information output and forcing of output file overwriting:
```
$ python3 -m fibrecnv -vf convert /home/sean/fibres
```
If you wish to store the output files in a different directory from the input files, specify the output path with the `-o` setting after the input directory:
```
$ python3 -m fibrecnv -v convert /home/sean/fibres -o /home/sean/fibres/output
```
If this directory does not exist, you will get an error:
```
Output directory is not writeable
```
To fix this, ensure the output directory exists and can be written to by the current user.

## Credits
Sean Leavey  
sean.leavey@glasgow.ac.uk